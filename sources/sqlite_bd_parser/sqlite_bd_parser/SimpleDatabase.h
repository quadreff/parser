#pragma once
#include <iostream>
#include "IDatabase.h"
class SQLiteDatabase {
	sqlite3 * db;
public:
	SQLiteDatabase() {};
	~SQLiteDatabase() { sqlite3_close(db); };
};


typedef char * DBErrorMessage;
////////////////////////////utils end//////////////////

class SimpleDatabase:IDatabase
{
	SQLiteDatabase _db;
	DBErrorMessage _db_error_msg;
public:
	SimpleDatabase(std::wstring path_to_db);
	virtual int Execute(std::string request);
	~SimpleDatabase();
};


