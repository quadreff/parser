#pragma once
#include <string>
#include "sqlite3.h"
class IDatabase
{
	virtual int Execute(std::string request) = 0;
};


