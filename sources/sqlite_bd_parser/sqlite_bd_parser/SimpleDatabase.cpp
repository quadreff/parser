#include "SimpleDatabase.h"

static int request_callback(void* NotUsed, int argc, char **argv, char **azColName)
{
	for (size_t i = 0; i < argc; ++i)
		std::cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << std::endl;
	std::cout << std::endl;
	return 0;
}


int SimpleDatabase::Execute(std::string request) 
{
	sqlite3_exec(_db, request.c_str(), request_callback, 0, &db_error_msg);


	return 0;
}